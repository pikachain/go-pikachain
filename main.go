package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"
	"strconv"

	"github.com/pikachain/go-pikachain/blockchain"
)

// CommandLine  manages the blockchain operations
type CommandLine struct {
}

// printUsage is triggered when the user fails to use the commands properly
func (cli *CommandLine) printUsage() {
	fmt.Println("Usage:")
	fmt.Println("getbalance -address ADDRESS -get the balance for the given address")
	fmt.Println("createblockchain -address ADDRESS creates the genesis block by the given address")
	fmt.Println("printchain - Prints the blockchain")
	fmt.Println("send -from ADDRESS -to ADDRESS - Transfer between 2 addresses")
}

// validateArgs is used to validate the commands sent by the user
func (cli *CommandLine) validateArgs() {
	if len(os.Args) < 2 {
		cli.printUsage()
		runtime.Goexit()
	}
}

// triggered when "print" command is used
func (cli *CommandLine) printChain() {
	chain := blockchain.ContinueBlockChain("")
	defer chain.Database.Close()

	iter := chain.Iterator()

	for {
		block := iter.Next()
		fmt.Println("---")
		fmt.Printf("Previous hash: %x\n", block.PrevHash)
		fmt.Printf("Hash of the block: %x\n", block.Hash)
		pow := blockchain.NewProof(block)
		fmt.Printf("PoW: %s\n", strconv.FormatBool(pow.Validate()))
		fmt.Println("")
		if len(block.PrevHash) == 0 {
			break
		}
	}
}

func (cli *CommandLine) createBlockChain(address string) {
	chain := blockchain.InitBlockChain(address)
	chain.Database.Close()
	fmt.Println("Just created a new blockchain!")
}

func (cli *CommandLine) getBalance(address string) {
	chain := blockchain.ContinueBlockChain(address)
	defer chain.Database.Close()

	balance := 0
	UTXOs := chain.FindUTXO(address)

	for _, out := range UTXOs {
		balance += out.Value
	}

	fmt.Printf("Balance of %s is %d\n", address, balance)
}

func (cli *CommandLine) send(from, to string, amount int) {
	chain := blockchain.ContinueBlockChain(from)
	defer chain.Database.Close()

	tx := blockchain.NewTransaction(from, to, amount, chain)
	chain.AddBlock([]*blockchain.Transaction{tx})
	fmt.Printf("Successfully sent %d tokens from %s to %s\n", amount, from, to)

}

// run manages the CLI
func (cli *CommandLine) run() {
	cli.validateArgs()

	getBalanceCmd := flag.NewFlagSet("getbalance", flag.ExitOnError)
	createBlockchainCmd := flag.NewFlagSet("createblockchain", flag.ExitOnError)
	sendCmd := flag.NewFlagSet("send", flag.ExitOnError)
	printChainCmd := flag.NewFlagSet("printchain", flag.ExitOnError)

	getBalanceAddress := getBalanceCmd.String("address", "", "The address of the wallet in context")
	createBlockchainAddress := createBlockchainCmd.String("address", "", "The address of the wallet used to create the genesis block")
	sendFrom := sendCmd.String("from", "", "Sender's wallet address")
	sendTo := sendCmd.String("to", "", "Receiver's wallet address")
	sendAmount := sendCmd.Int("amount", 0, "Amount to be sent")

	switch os.Args[1] {
	case "getbalance":
		err := getBalanceCmd.Parse(os.Args[2:])
		handleCliError(err)
	case "createblockchain":
		err := createBlockchainCmd.Parse((os.Args[2:]))
		handleCliError(err)
	case "send":
		err := sendCmd.Parse(os.Args[2:])
		handleCliError(err)
	case "printchain":
		err := printChainCmd.Parse(os.Args[2:])
		handleCliError(err)
	default:
		cli.printUsage()
		runtime.Goexit()
	}

	if getBalanceCmd.Parsed() {
		if *getBalanceAddress == "" {
			getBalanceCmd.Usage()
			runtime.Goexit()
		}
		cli.getBalance(*getBalanceAddress)
	}

	if createBlockchainCmd.Parsed() {
		if *createBlockchainAddress == "" {
			createBlockchainCmd.Usage()
			runtime.Goexit()
		}
		cli.createBlockChain(*createBlockchainAddress)
	}

	if sendCmd.Parsed() {
		if *sendFrom == "" || *sendTo == "" || *sendAmount == 0 {
			sendCmd.Usage()
			runtime.Goexit()
		}
		cli.send(*sendFrom, *sendTo, *sendAmount)
	}

	if printChainCmd.Parsed() {
		cli.printChain()
	}
}

func main() {
	defer os.Exit(0)
	cli := CommandLine{}
	cli.run()
}

func handleCliError(err error) {
	if err != nil {
		log.Panicln(err)
	}
}
